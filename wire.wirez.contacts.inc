<?php

/**
 * @file
 *  Wirez comunications system, users
 * The requests must include:
 *  who: Who you are looking for in the form of name::example.com
 *  from: Who is doing the request in any form (name or name::example.com)
 * @todo
 *  Verify that it is an account from this place with the domain name in the request
*/

global $wire;

//Account update notifications
///@todo Confirm the status of this user and return a correct message
if($_GET['what'] == 'profileupdate' && $_GET['who']){

	//Find out the cid and confirm that the contact exists
	$contact = $wire->get_contact_info($_GET['who']);

	if($contact){

		$wire->markforupdate_contact($contact['cid']);

		$wire->_return('ALL_FINE', TRUE);

	}
	else{

		$wire->_return('ACCOUNT_DOES_NOT_EXIST', TRUE);

	}

}

//Return information about contacts
if($_GET['what'] == 'contactdetails' && $_GET['who']){

	//Get the name part of the address
	$name = explode('::', $_GET['who']);

   //Confirm the account
	if(wire_confirm_account($name[0], 1) == 0){
		$wire->_return('ACCOUNT_DOES_NOT_EXIST', TRUE);
	}

	//Get only public information, nothing internal like the uid
	$contact = $wire->get_contact_info($_GET['who'], 'external');

	if($contact){
		$wire->_return($contact);
	}

	$wire->_return('ACCOUNT_DOES_NOT_EXIST', TRUE);

}

//If it reaches this point there is not really much to say, but we'll deny the information anyway
$wire->_return('INFORMATION_REQUEST_DENIED', TRUE);


