<?php

/**
 * @file
 *  Wirez comunications system, relationships
 * The requests must include:
 * @todo
 *  Verify if the request is made from the correct host in case of the plain 'name' form
 *  Verify that it is an account from this place with the domain name in the request
*/

global $wire;

//I am only starting to test this function, if it works I'll use it more
function confirm_request($get){
	foreach($get as $gets => $g){
		if(!isset($_GET[$g]) || $_GET[$g] == ''){
			return FALSE;
		}

	}

	return TRUE;

}

if(!confirm_request(array(
	'to', 'from', 'relationship', 'type'
	))){
	$wire->_return('INFORMATION_REQUEST_DENIED', TRUE);
}

//Get this users uid
$to = explode('::', $_GET['to']);

$account = wire_confirm_account($to[0], 1);

//Confirm the account exists and that it is active
if(!$account){
	wire_return('ACCOUNT_DOES_NOT_EXIST', TRUE);
}

//Get the public information about this account, if exists
$contact = $wire->get_contact_info($_GET['from']);

if(!$contact){

	//Create the contact, update information later
	if($_GET['type'] == 'apply'){

		$info['uid']          = 0;
		$info['wire_address'] = $_GET['from'];
		$info['name']         = '';
		$info['unique_id']    = '';
		$info['about']        = '';
		$info['token']        = '';
		$info['picture']      = '';
		$info['status']       = 1;

		$wire->create_contact($info);

		//Retrieve the 'new' information
		$contact = $wire->get_contact_info($_GET['from']);

	}
	else{
		$wire->_return('ACCOUNT_DOES_NOT_EXIST', TRUE);
	}

}

//New relationship
if($_GET['type'] == 'apply'){

	if(!$wire->confirm_rel($contact['cid'], $account['uid'], ($_GET['relationship'] == 'follow' ? 2 : 3))){

		//Create the relationship
		$r = $wire->rels_tasks(($_GET['relationship'] == 'contact' ? 3 : 2), 0, $contact['wire_address'], $account['uid'], FALSE, FALSE, FALSE);

		//Approval token, done in two steps so that the system can send the notifications
		$this_user = $wire->get_contact_info($_GET['to']);

		//Auto-approve relationship
		if($_GET['token'] == $this_user['token'] || $this_user['token'] == ''){
			$r = $wire->rels_tasks(($_GET['relationship'] == 'contact' ? 3 : 2), 1, $contact['wire_address'], $account['uid'], wire_member_name($account['name']), TRUE, FALSE);

			//Send the notification
			$wire->rel_notification($_GET['relationship'], $_GET['to'], $_GET['from'], 'validate');
		}

		//All fine
		if($r){
			$wire->_return('ALL_FINE', TRUE);
		}
	}
	else{
		//There should be a warning or something, because the request has already been done
		$wire->_return('RELATIONSHIP_EXISTS', TRUE);
	}

}

//I can only validate follow (type 1) requested relationships
if($_GET['type'] == 'validate'){

	if($wire->confirm_rel($contact['cid'], $account['uid'], ($_GET['relationship'] == 'follow' ? 1 : 3))){

		$r = $wire->rels_tasks(($_GET['relationship'] == 'contact' ? 3 : 1), 1, $contact['wire_address'], $account['uid'], FALSE, FALSE, FALSE);

		//All fine
		if($r){
			$wire->_return('ALL_FINE', TRUE);
		}

	}
	elseif($rel['status'] == 1){
		$wire->_return('RELATIONSHIP_NOT_PENDING', TRUE);
	}
	else{
		$wire->_return('RELATIONSHIP_DOES_NOT_EXIST', TRUE);
	}

}

if($_GET['type'] == 'reject'){

	$extra = ($_GET['extra'] == 2 ? 1 : 2);

	//Special case when following

	//Confirm relationship, it should only exist, it does not matter in which status it is
	if($wire->confirm_rel($contact['cid'], $account['uid'], ($_GET['relationship'] == 'follow' ? $extra : 3))){
		$r = $wire->rels_tasks(($_GET['relationship'] == 'contact' ? 3 : $extra), 3, $contact['wire_address'], $account['uid'], FALSE, FALSE, FALSE);
	}
		else{
			$wire->_return('RELATIONSHIP_DOES_NOT_EXIST', TRUE);
		}

	//All fine
	if($r){
		$wire->_return('ALL_FINE', TRUE);
	}

}

