<?php

/**
 * @file
 *  Wirez comunications system, messages
 *  who: for whom is the message, hash: message unique id code; from: who sends the message
 * @todo
 *  Confirm that the message comes from who it says it is comming from, with the key at least
 * @todo
 *  Confirm that the message is for someone actually hosted here
 * @todo
 *  Let the user decide if s/he wants to accept more than one message from not-confirmed contacts
 * @todo
 *  Update the recieved message for the recipient??
 * @todo
 *  Let the user decide if s/he wants to receive rewired messages from unknown authors, also the admin
 * @todo
 *  Let people who I'm contacts with post to my wire. Or let me decide.
*/

global $wire;

if(!isset($_GET['to'])  || !isset($_GET['mkey']) || !isset($_GET['from']) || !isset($_GET['timestamp'])){
	$wire->_return('INFORMATION_REQUEST_DENIED', TRUE);
}

/**
 * Stores a message in the q in order to be retrieved
 */
function wire_message_send_q($author, $uid, $contact){

	global $wire;

	$form['contact_cid'] = $contact;
	$form['text']        = '';
	$form['parent']      = 0;
	$form['mkey']        = $_GET['mkey'];
	$form['timestamp']   = $_GET['timestamp'];
	$form['commkey']     = (isset($_GET['commkey']) ? $_GET['commkey'] : NULL);
	$form['type']        = $_GET['type'];
	$form['channel']     = $_GET['channel'];
	$form['status']      = 1;
	$form['uid']         = $uid;
	$form['author']      = $author;
	$form['attachments'] = '';

	return $wire->add_message($form);

}

$to = explode('::', $_GET['to']);

//Confirm that this account exists in this site
$account = wire_confirm_account($to[0], 1);

if(!$account){
	$wire->_return('ACCOUNT_DOES_NOT_EXIST', TRUE);
}

//Get the account information
$account = $wire->get_contact_info($_GET['to']);

//Get the contact information
$contact = $wire->get_contact_info($_GET['from']);

//By default the user will only receive messages to the wire from people s/he follows, but there can be direct messages to the wire, s/he should be able to select who can post to the personal wire. In this case, ONLY people who I follow can post to my wire
$rel_type = ($_GET['channel'] == 'pm' ? 3 : 1);

/* Pending for when The contacts get created with the first message
		$info['uid']          = 0;
		$info['wire_address'] = $_GET['from'];
		$info['name']         = '';
		$info['unique_id']    = '';
		$info['about']        = '';
		$info['token']        = '';
		$info['picture']      = '';
		$info['status']       = 1;

		$wire->create_contact($info);
		//Retrieve the 'new' information
		$contact = $wire->get_contact_info($_GET['from']);

 */

//If the conection does not exist, reject the message
//This may have a setting given by the admin or the user, if a new conection may be requested in this cases.
if(!$wire->confirm_rel($contact['cid'], $account['uid'], $rel_type) || !$contact){
	$wire->_return('CONTACT_DOES_NOT_EXIST', TRUE);
}

//Wires and Rewires
if($_GET['channel'] == 'w' && isset($_GET['author'])){

	//Confirm that the author resides here, if not, we'll create an account for it and confirm it later on
	$author = $wire->get_contact_info($_GET['author']);

	//create contact and get its details
	if(!$author){

		$info = array(
					'wire_address' => $_GET['author'],
					'status'       => 1
					);

		$wire->create_contact($info);
		$author = $wire->get_contact_info($_GET['author']);
	}
}
else{
	//The author is who ever is sending the message
	$author = $contact;
}

//We need a setting where the user may select to store messages on Q, for now we'll assume that s/he wants to do it
if($status == 0 || $status == 4){
	//wire_return(array(CONTACT_PENDING_CONFIRM));
}

$m = wire_message_send_q($author['cid'], $account['uid'], $contact['cid']);

if($m){

	//Retrieve the message at once
	$messages = $wire->messages_update_remote($account['uid'], $contact['cid'], $account['wire_address']);

	$wire->_return('MESSAGE_RECIEVED', TRUE);

}

//If something goes wrong we'll return an error
$wire->_return('INFORMATION_DENIED', TRUE);

