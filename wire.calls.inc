<?php

/**
 * @file
 *  Wired conversations file. This is in charge of loading (ajax refreshed) the current conversation and a block with a list of contacts with pending messages.
 * @todo Change this file because it is not the best way to handle this calls, not very secure.
 * GET it order to avoid repeating processes here, everything must be sent, we'll not make assumtions here
 *  - uid
 *  - cid
 *  - mode
 *  - parent
 *  - user (wire_address)
 *  - contact (wire_address)
 *  - ini
 *  - [end]
 *  - channel
 */

//Get module parts
drupal_load('module', 'wire');
module_load_include('inc', 'wire', 'wire.conversation');

/**
 * Special function just to view someones wire 
 * It is very similar to _wire_conversation_get, but more restricted.
 */
function _wire_get_public(){

	global $wire;

	//Get the contact id of this user
	$contact = $wire->get_contact_info(wire_member_name($_GET['user_name']));

	if(!$_GET['user_name'] || $_GET['user_name'] == '' || !$contact || $contact['uid'] == 0){ 
		print "<div class='wire_wire_not_found'>" . t("No user found") . "</div>";
		return;
		//return MENU_NOT_FOUND;
	}


	//Some stuff required to get the messages
	$stuff['mode']    = 0;
	$stuff['channel'] = 'w'; //Channel
	$stuff['parent']  = 0; //Parent
	$stuff['ini']     = $_GET['ini'] ? $_GET['ini'] : 0; //Where to start

	//Get a list of all conversations marked for the wire
	$messages = $wire->get_messages($contact['uid'], $contact['cid'], $stuff);

	if($messages){
		$output .= wire_theme_conversation($messages, 'w');
	}
	else{
		$output = t("This person has not posted anything in the Wire");
	}

	print $output;
}

/**
 * Function to retrieve messages in conversation
 * All the required information must be received in order to avoid the same checks already done
 */
function _wire_conversation_get(){

	global $user, $wire;

	//Popup window to display media
	$output = "<html><head>
		<script language='javascript' type='text/javascript'>
		<!--
		function popitup(url, name, size) {
			newwindow=window.open(url, name, size);
			if (window.focus) {newwindow.focus()}
			return false;
		}
		// -->
		</script>

		<link type='text/css' rel='stylesheet' media='all' href='/". drupal_get_path('module', 'wire') . "/wire.css' />

		</head><body>
	";

	//Admins case
	if(wire_member_name() != $_GET['user'] && !user_access('admin wires')){
		return MENU_ACCESS_DENIED;
	}

	$output .= ('Last check was at: ' . date('H:i:s', time())) . "<br>";

	$alert = '<br><br><div class="wire_no_messsages">' . t('There are no messages,<br> try saying something ;-)') . "<br></div><br /><br />";

	//Some stuff for the message retrieving part
	//Display type
	$stuff['mode']    = $_GET['mode'];
	$stuff['channel'] = $_GET['channel']; //Channel
	$stuff['parent']  = $_GET['parent']; //Parent
	$stuff['ini']     = $_GET['ini'] ? $_GET['ini'] : 0; //Where to start

	//Display tipe
	if($mode == VIEW_TYPE_THREADED){
		$stuff['parent'] = 0;
	}

	//Talking to self
	if($_GET['user'] == $_GET['contact']){

		$me = $contact = $wire->get_contact_info($_GET['user']);

	}
	else{

		$contact = $wire->get_contact_info($_GET['contact']);
		$me      = $wire->get_contact_info(wire_member_name());

	}

	//Assume there are none
	$messages = FALSE;

	//Private Messaging
	if($_GET['channel'] == 'pm'){

		//get a list of messages for this pair
		$messages = $wire->get_messages($_GET['uid'], $_GET['cid'], $stuff);

		if($messages){
			$output .= wire_theme_conversation($messages);
		}

		$wire->update_contact_visits($_GET['uid'], $_GET['cid'], 'pm');

	}
	else{
		//Get a list of all conversations marked for the wire
		$messages = $wire->get_messages($_GET['uid'], $_GET['cid'], $stuff);

		if($messages){
			$output .= wire_theme_conversation($messages, 'w');
		}

		$wire->update_contact_visits($user->uid, 0, 'wire');
	}

	//If there are no messages and there is a parent indicated, we'll display it.
   //It should also appear at the end of any conversation, at least when the last message is retreived.
	if(!$messages && $_GET['parent']){
		$messages = $wire->get_messages($_GET['uid'], $_GET['cid'], array('mkey' => $_GET['parent']));

		$output .= wire_theme_conversation($messages, 'w');
	}

	//Set the no message warning if there are no messages
	if(!$messages){
		$output .= $alert;
	}

	print $output . "</body></html>";
}


/**
 * Gets a list of new contacts that has sent messages
 * @todo
 *  Let the admin select the columns and ammount of contacts to retrieve
 * @todo
 *  Correct the size of the avatar
 */
function _wire_conversation_get_contacts_new_messages(){

	global $user, $wire;

	$output = FALSE;

	//Get a list of new messages for this user
	$messages = $wire->messages_get_new($user->uid, 'wire');

	foreach($messages['wire'] as $message => $m){

		$link = url('wire');

		$title = sprintf(t('You have %d messages in your wire'), $m['total_messages']);

		$m['total_messages'] = $m['total_messages'] == '' ? 0 : $m['total_messages'];

		$number = ($m['total_messages'] < 10 ? $m['total_messages'] : '9plus');

		$output .= '<a href="'.$link.'"><div style="height: 50px; width: 50px; background-image: url(/' . drupal_get_path('module', 'wire') . '/imgs/wire_other_avatar.png' . '); no-repeat;" title="'.$title.'"><img src="/' . drupal_get_path('module', 'wire') . '/imgs/number-'.$number.'.png' . '" title="'.$title.'" alt="wire alert new messages"></div></a>';

	}

	//Now each contact, avoid warning about the current contact
	$messages = $wire->messages_get_new($user->uid, 'pms');

	foreach($messages['pm'] as $messages => $m){

		if($m['cid'] != $_GET['cid']){
			$link = url('wire/talk', array('query' => array('wire_address'=> $m['wire_address'])));

			$title = sprintf(t('You have %d messages from %s'), $m['messages'], ($m['name'] ? $m['name'] : $m['wire_address']));

			$number = ($m['messages'] < 10 ? $m['messages'] : '9plus');

			$output .= '<a href="'.$link.'"><div id="wireNewMessageContainer">
								<div id="wireNewMessageLeft">
									<img src="/' . drupal_get_path('module', 'wire') . '/imgs/number-'. $number . '.png' . '" title="'.$title.'">
								</div>
								<div id="wireNewMessageRight">
									<img src="' . wire_picture($row['picture']) . '" title="'.$title.'" alt="' . $title . '" style="height: 30px; width: 30px;">
								</div>
							</div></a>';

			//$output .= '<a href="'.$link.'"><div style="height: 50px; width: 50px; background-image: url(/' . wire_picture($row['picture']) . '" title="'.$title.'"><img src="/' . drupal_get_path('module', 'wire') . '/imgs/number-'.$number.'.png' . '" title="'.$title.'" alt="New Messages From '.$row['name'].'"></div></a>';

		}

	}

	print $output;

}

