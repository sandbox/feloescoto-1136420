<?php

/**
 * @file
 *  Wirez comunications system, retrieves messages pending
 * @todo
 *  confirm that the request is been made from the actual host, we'll need to connect to the central server in case of plain accounts
 * @todo
 *  confirm that the message was sent from the sender indicated
*/

global $wire;

if(!isset($_GET['to']) || !isset($_GET['mkey']) || !isset($_GET['from'])){

	$wire->_return('INFORMATION_REQUEST_DENIED', TRUE);

}

//Lets find the uid of the messenger, in this case, from is who originally sent the message
$from = explode("::", $_GET['from']);

//Confirm the account in this system
$account = wire_confirm_user($from[0]);

if(!$account){
	$wire->_return('ACCOUNT_DOES_NOT_EXIST', TRUE);
}

//Get the details of the account
$account = $wire->get_contact_info($_GET['from']);

//Get details about the contact
$contact = $wire->get_contact_info($_GET['to']);

if(!$contact){
	$wire->_return('CONTACT_DOES_NOT_EXIST', TRUE);
}

//There is need to confirm the contact id in the message only if it is a pm, otherwise, it only matters that the contact is following
$cid = ($_GET['channel'] == 'pm' ? $contact['cid'] : FALSE);

//Retrieve the message
$msg = $wire->return_messages($account['uid'], $cid, array('mkey' => $_GET['mkey']), 0);

$wire->_debug($msg, 'Return');

if(!$msg){
	$wire->_return('MESSAGE_DOES_NOT_EXIST', TRUE);
}

$wire->_return($msg);

