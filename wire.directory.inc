<?php
/**
 * @file
 *  Wired directory manager
 */

/**
 * Helper function to find contacts
 */
function _wire_dir_find($what, $settings = array(), $dir = FALSE){

	global $wire, $user;

	//Just in case
	$output = "<h2>Your query did not return any results, try something else please</h2>";

	//Lest see where to start
	$ini = $_GET['ini'] ? $_GET['ini'] : 0;

	//Lest see where to end
	$end = $_GET['end'] ? $_GET['end'] : 10;

	$results = $output = NULL;

	//Search at the hive
	if($dir && $what != ''){

		$options = array(
			'ini'          => $ini,
			'end'          => $end,
			'who_searches' => wire_member_name($settings['who_searches']),
			'uid'          => $user->uid
			);

		$results = $wire->hive_search_contacts($what, $options);

      //Try to get the contact information from its corresponding website if nothing was found
      if(!$results && $wire->name_type($what) == 3){ #required php 5.3 to access the class constants in an easy way, we'll wait
         $results = array(); #It gets converted to a string for which it gets into trouble in the next line, it need to be an array

			$results[] = $wire->profiles_get_remote($what); 

			//Confirm the correct response
         if($results[0] == '' || is_numeric($results[0])){
            $results = FALSE; #I'll just return the value to false and deal with the reports internally, the user doesn't really need to know the internals
         }
		}

		if($results && !is_numeric($results)){
			$output = _wire_theme_contact_list($results, "Select a contact");
		}

	}
	//Internal directory searches, we'll assume it is for personal contacts
	elseif(!$dir){

		global $user;

		$options = array(
			'ini'  => $ini,
			'end'  => $end,
			'uid'  => $user->uid,
			'type' => $_GET['rel'] != 0 ? 'relationships' : $settings['type'],
			'who_searches' => wire_member_name($user->name),
			'rel' => $_GET['rel']
			);

		$results = $wire->search_contacts($what, $options);

		if($results){
			$output = _wire_theme_contact_list($results, "Select a contact");
		}
	}

	return $output;

}

/**
 * Contacts requests forms
 * @todo
 *  Do not add options for existent relationships
 */
function _wire_form_contact_request(&$form_state, $details, $info){

	$form = array();

	$form['wire_contact'] = array(
		'#type' => 'fieldset',
		'#title' => t('Relationships'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);

	$form['wire_contact']['wire_contacts_token'] = array(
		'#type' => 'textfield',
		'#title' => t('Token'),
		'#size' => 20,
		'#description' => t('Add a secret token for auto-approval of this request if you have one.'),
	);

	$form['wire_contact']['wire_contact_type'] = array(
		'#type' => 'radios',
		'#options' => array(3 => t('Contact'), 1 => t('Follow')),
		'#default_value' => 3,
	);

	$form['wire_contact']['wire_contact_address'] = array(
		'#type' => 'hidden',
		'#default_value' => $details['wire_address'],
	);

	$form['wire_contact']['wire_contact_info'] = array(
		'#type' => 'hidden',
		'#default_value' => $info
	);

	$form['wire_contact']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit Request'),
	);

	return $form;
}

/**
 * Process the Contacts requests forms
 */
function _wire_form_contact_request_submit($form, &$form_state){

	global $user, $wire;

	$query = array(
		'task'    => $form_state['values']['wire_contact_type'],
		'uid'     => $user->uid,
		'contact' => $form_state['values']['wire_contact_address'],
		'status'  => 4,
		'rels'    => TRUE,
	);

	if($form_state['values']['wire_contacts_token']){
		$query['token'] = $form_state['values']['wire_contacts_token'];
	}

	//I'll create the contact from here, with the information that we have already have,
	//it will be marked for update next time somebody visits him/her just in case

	//Extract the information from the form
	$contact_info = json_decode($form['wire_contact']['wire_contact_info']['#default_value']);

	//Confirm that it is not an internal user
	$address = explode('::', $contact_info->wire_address);

	if($address[1] != $_SERVER['SERVER_NAME']){

		//We only have to do this if it is not an internal contact or if the contact does not exist
		$contact = $wire->get_contact_info($form['wire_address']);
		
			$info['uid']          = 0;
			$info['wire_address'] = $contact_info->wire_address;
			$info['name']         = $contact_info->name;
			$info['unique_id']    = $contact_info->unique_id;
			$info['about']        = $contact_info->about;
			$info['token']        = $contact_info->token;
			$info['picture']      = $contact_info->picture;
			$info['status']       = 1;

		//if not lets create it
		if(!$contact){
			
			$wire->create_contact($info);
		}
		else{
			$wire->update_contact($info);
		}
	}

	drupal_goto('wire/contacts/tasks', $query);

}

/**
 * Form to find contacts
 */
function _wire_dir_form_contact_find(&$form_state){

	$form['wire_find'] = array(
		'#type' => 'fieldset',
		'#title' => t('Search for contacts'),
		'#description' => t('Use ::name or tux::example.com or just a name like "Tux the penguin" <br> <a href="http://wirez.me/help/search">More help</a>'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE
	);

	$form['wire_find']['wire_contacts_find'] = array(
		'#type' => 'textfield',
		'#title' => t('Find a contact'),
		'#size' => 50,
		'#default_value' => $_GET['who']
	);

	$form['wire_find']['directory_fieldset'] = array(
		'#type' => 'fieldset',
		'#title' => t('Where to look'),
	);

	$form['wire_find']['directory_fieldset']['directory'] = array(
		'#type' => 'radios',
		'#default_value' => (isset($_GET['dir']) && $_GET['dir'] == 1 ? 0 : 1),
		'#options' => array(t('Use Directory'), t('Stay Local'))
	);

	$form['wire_find']['contact_type_fieldset'] = array(
		'#type' => 'fieldset',
		'#title' => t('Relationship'),
		'#description' => t('This settings only apply if you perform a local search')
	);

	$form['wire_find']['contact_type_fieldset']['contact_type'] = array(
		'#type' => 'radios',
		'#default_value' => 3,
		'#options' => array(0 => t('Any One'), 3 => t('Contacts'), 1 => t('Following'), 2 => t('Followers'))
	);

	$form['wire_find']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Find'),
	);

	return $form;
}

/**
 * Form to find contacts
 */
function _wire_dir_form_contact_find_submit($form, &$form_state){

	//If you are looking for someone
	global $find;

	$dir = $form_state['values']['directory'] == FALSE ? TRUE : '';

	$rel = FALSE;

	//Select a relationship in case of looking in house
	if(!$dir){
		$rel = $form_state['values']['contact_type'];
		}

	if($form_state['values']['wire_contacts_find'] != '' || !$dir){
		drupal_goto('wire/directory/view/find/', array('who' => $form_state['values']['wire_contacts_find'], 'dir' => $dir, 'rel' => $rel));
	}

}

/**
 * Creates a table with a contact list, this could use the theme functionality
 */
function _wire_theme_contact_list($info, $title){

	global $user;

	$output = "<h2>".$title."</h2>";
	$output .= "<table>";

	foreach($info as $information => $i){

		$query = array('wire_address' => $i['wire_address']);

		$talk = l('Talk', 'wire/talk/', array('query' => $query));
		$wire = l('Wire', 'wire/', array('query' => $query));

		$output .= '<tr>
						<td width="100" valign="top">
							<img src="' . wire_picture($i['picture']) . '" class="wire_avatar">
						</td>';
		$output .= "<td valign='top' width='50%'>".
						sprintf('<h2> %s </h2> %s %s | %s <br> %s <br> %s',
						($i['name'] != '' ? $i['name'] : t('Anonymous')),
						($user->uid == 1 ? $i['wire_address'] . '<br>' : ''),
						$talk,
						$wire,
						$i['about'],
						$status) .
						wire_contact_theme_tasks($i['relationships'], $i['wire_address'], $i['wire_address']) . "
						</td><td>";

		$output .= drupal_get_form('_wire_form_contact_request',
					  array('wire_address' => $i['wire_address']),
					  json_encode($i));

		$output .= "</td>
					</tr><tr colspan=3><td colspan=3><hr/></td></tr>";
	}

	$output .= "</table>";

	if ($no_results) {
		$output = '<p>' . t('No results found!') . '</p>';
	}

	return $output;

}

/**
 * Generates a list of tasks for a contact based on its current status
 */
function wire_contact_theme_tasks($rels, $wire_address, $contact, $uid = FALSE){

	if(!$uid){
		global $user;
		$uid = $user->uid;
	}

	if(!is_array($rels)){
		$rels = array();
	}

	$path = 'wire/contacts/tasks';

	$follow_link = $follower_link = $contact_link = FALSE;

	foreach($rels as $rel => $r){

		//This user Follows the contact
		if($r['type'] == 1){
			//Pending
			if($r['status'] == 0){
				$follow_link ='Your requested to follow this contact (pending) | ';
				$follow_link .= _wire_contact_theme_tasks_links('Stop Request', $path, 1, 3, $uid, $contact);
				$follow_link .= "<br>";
			}
			elseif($r['status'] == 1){
				$follow_link ='You are Following this contact | ';
				$follow_link .= _wire_contact_theme_tasks_links('Stop', $path, 1, 3, $uid, $contact);
				$follow_link .= "<br>";				
			}
			elseif($r['status'] == 4){
				$follow_link ='You requested to Follow this contact (Pending Approval) | ';
				$follow_link .= _wire_contact_theme_tasks_links('Stop', $path, 1, 3, $uid, $contact);
				$follow_link .= "<br>";				
			}
		}

		//This contact is a follower of the user
		if($r['type'] == 2){
			//Pending
			if($r['status'] == 0){
				$follower_link = 'This contact wants to follow you | ';
				$follower_link .= _wire_contact_theme_tasks_links('Accept | ', $path, 2, 1, $uid, $contact);
				$follower_link .= _wire_contact_theme_tasks_links('Reject', $path, 2, 3, $uid, $contact);
				$follower_link .= "<br>";
			}
			elseif($r['status'] == 1){
				$follower_link = 'This contact is Following you | ';
				$follower_link .= _wire_contact_theme_tasks_links('Stop it', $path, 2, 3, $uid, $contact);
				$follower_link .= "<br>";				
			}
		}
		//Contacts to you
		if($r['type'] == 3){
			//Pending
			if($r['status'] == 0){
				$contact_link  = 'Contact request pending | ';
				$contact_link .= _wire_contact_theme_tasks_links('Accept | ', $path, 3, 1, $uid, $contact);
				$contact_link .= _wire_contact_theme_tasks_links('Reject', $path, 3, 3, $uid, $contact);
				$contact_link .= "<br>";
			}
			elseif($r['status'] == 1){
				$contact_link  = 'You are a contact | ';
				$contact_link .= _wire_contact_theme_tasks_links('Stop', $path, 3, 3, $uid, $contact);
				$contact_link .= "<br>";				
			}
			elseif($r['status'] == 4){
				$contact_link  = 'You requested to be a contact(Pending approval) | ';
				$contact_link .= _wire_contact_theme_tasks_links('Remove request', $path, 3, 3, $uid, $contact);
				$contact_link .= "<br>";
			}
		}
	}

	return $follow_link . $follower_link . $contact_link . $contact_form;

}

/**
 * Creates a link to update relationships
 */
function _wire_contact_theme_tasks_links($text, $path, $task, $status, $uid, $contact){

	$query = array(
		'task'    => $task,
		'uid'     => $uid,
		'contact' => $contact,
		'status'  => $status,
		'rels'    => TRUE,
	);
	//return l(t($text), $path . $task . '/' . $uid . '/'. $contact);
	return l(t($text), $path, array('query' => $query));
	//path . task . status . uid . cid . wire_name
	//rel: 1 follow, 2 follower, 3 and 4 contact
	//status: 0: pending, 1: approve or active, 3: reject

}

/**
 * Main function to look for contacts
 */
function wire_directory(){

	global $user;

	drupal_set_title(t('Wire Directory'));

	$content = drupal_get_form('_wire_dir_form_contact_find');

	$contacts = FALSE;

	$content .= sprintf("<div>Filter only: %s | %s | %s</div>",
						l('Contact',   'wire/directory/view/find', array('query' => array('rel' => 3))),
						l('Following', 'wire/directory/view/find', array('query' => array('rel' => 1))),
						l('Followers', 'wire/directory/view/find', array('query' => array('rel' => 2))));

	if(arg(2) == 'find'){
		$contacts = _wire_dir_find($_GET['who'], array('who_searches' => $user->name), $_GET['dir']);
	}
	else{
		//We'll locate only contacts if there is nothing to be searched
		$contacts = _wire_dir_find(($_GET['who'] ? $_GET['who'] : ''), array('who_searches' => $user->name, 'type' => 'relationships'));
	}

	if($contacts){
		$content .= $contacts;
	}
	else{
		$content .= t("<br/><br/><br/><h2>There are no contacts to display, you may use the form above to search for some</h2>");
	}

	return $content;

}

