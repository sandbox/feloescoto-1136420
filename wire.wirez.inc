<?php

/**
 * @file
 *  Wirez comunications system, base for remote connections
 * @todo
 *  Make the communication key optional
*/

global $wire;

///Confirm the communications key
if(!_wire_variable_get('wire_only_with_comm_key', 0)){
	if(!$wire->confirm_comm_key($_GET['commkey'], _wire_variable_get('wire_min_score_comm_client', 50))){
		$wire->_return('INFORMATION_REQUEST_DENIED', TRUE);
	}
}

if($_GET['what'] == 'contactdetails' || $_GET['what'] == 'profileupdate'){
	module_load_include('inc', 'wire', 'wire.wirez.contacts');
}
elseif($_GET['what'] == 'newmessage'){
	module_load_include('inc', 'wire', 'wire.wirez.messages');
}
elseif($_GET['what'] == 'messageretrieve'){
	module_load_include('inc', 'wire', 'wire.wirez.messages.retrieve');
}
elseif($_GET['what'] == 'relationship'){
	module_load_include('inc', 'wire', 'wire.wirez.relationship');
}
elseif($_GET['what'] == 'deletemessage'){
	module_load_include('inc', 'wire', 'wire.wirez.messages.delete');
}

