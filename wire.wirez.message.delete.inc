<?php

/**
 * @file
 *  Wirez comunications system, deletes remote messages
 */

global $wire;

//Confirm required information
if(!isset($_GET['to'])  || !isset($_GET['hash']) || !isset($_GET['from']) || !isset($_GET['time'])){
	$wire->_return('INFORMATION_DENIED', TRUE);
}

//Confirm that the user exists
$to = explode('::', $_GET['to']);

if(!wire_confirm_account($to[0])){
	$wire->_return('ACCOUNT_DOES_NOT_EXIST', TRUE);
}

//Get the contact information, we'll delete the message even if there is no relationship anymore
$contact = $wire->get_contact_info($_GET['from']);

//Get the message and confirm that the contact who sent the message is the one telling you to delete it
$message = $wire->get_messages(FALSE, $contact['cid'], array('mkey' => $_GET['mkey']));

$wire->delete_messages($message);



