<?
/**
 * @file
 *  Wired Messages Search Page
 */

/**
 * Search messages form
 */
function _wire_messages_search_form(){
	
	global $wire;

	$form = array();

	$member = $wire->get_contact_info(wire_member_name());

	$form['wire_messages_search'] = array(
		'#type' => 'textfield',
		'#title' => t('Text to search'),
		'#default_value' => $_GET['text'],
		'#size' => 50,
		'#description' => t('Type in something that you want to search'),
	);

	$form['wire_search_type'] = array(
		'#type' => 'radios',
		'#options' => array(t('Only in my messages'), 1 => t('The whole world')),
		'#default_value' => 1,
	);

	$form['wire_member_address'] = array(
		'#type' => 'hidden',
		'#default_value' => $member['wire_address'],
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Search'),
	);

	return $form;

}

/**
 * Process the search form
 */
function _wire_messages_search_form_submit($form, &$form_state){

	if($form_state['values']['wire_messages_search'] != ''){
		drupal_goto('wire/search/messages', array('text' => $form_state['values']['wire_messages_search'], 'search_type' => $form_state['values']['wire_search_type']));
	}

}

/**
 * Helper function to theme messages returned from the hive
 */
function _wire_theme_messages_remote($m){

	global $wire;

	//Find this contact here
	$contact = $wire->get_contact_info($m['contact_info']['wire_address']);

	//If contact does not exist, create it
	if(!$contact){

		$info['uid']          = 0;
		$info['wire_address'] = $m['contact_info']['wire_address'];
		$info['name']         = $m['contact_info']['name'];
		$info['unique_id']    = '';
		$info['about']        = $m['contact_info']['about'];
		$info['token']        = '';
		$info['picture']      = $m['contact_info']['picture'];
		$info['status']       = 1; //Just in case, we'll retreive the information the next time somebody tries to get in touch with him/her

		$wire->create_contact($info);

		$contact = $wire->get_contact_info($m['author_info']['wire_address']);

	}

	//Author information, but first lets see if it is different from the contact
	if($m['author_info']['wire_address'] != $m['contact_info']['wire_address']){

		$author = $wire->get_contact_info($m['author_info']['wire_address']);

		//If the author does not exist, create it, if it is diffrent from the contact
		if(!$author){

			$info['uid']          = 0;
			$info['wire_address'] = $m['author_info']['wire_address'];
			$info['name']         = $m['author_info']['name'];
			$info['unique_id']    = '';
			$info['about']        = $m['author_info']['about'];
			$info['token']        = '';
			$info['picture']      = $m['author_info']['picture'];
			$info['status']       = 1; //Just in case, we'll retreive the information the next time somebody tries to get in touch with him/her

			$wire->create_contact($info);

			$contact = $wire->get_contact_info($m['author_info']['wire_address']);

		}

	}
	else{ //if they are the same
		$author = $contact;
	}

	//Rewire link
	$rewire = l("Rewire", "wire/rewire/message/" . $m['mkey'] . "/" . $contact['cid']);

	//Posted by link
	$posted_by = l(($author['name'] ? $author['name'] : "[ " . $author['wire_address'] . " ]"), 'wire/directory/view/find/', array('query' => array('who' => $author['wire_address'], 'dir' => TRUE)));

	//Talk to contact link
	$talk = l('Talk', 'wire/talk/', array('query' => array('wire_address' => $author['wire_address'])));

	//Avatar image format
	$avatar = "<img src='" . wire_picture($author['picture']) . "' alt='Avatar".$author['name']."' title='" . $author['name'] . "'>";

	//Link to view the details about this contact
	$dir_link = url('wire/directory/view/find', array('query' => array('who'=> $author['wire_address'], 'dir' => TRUE)));

	//Link with information about the client used to send the message
	$via = l($m['communication_via'], $m['communication_url'], array('attributes' => array('title' => 'Posted via ' . $m['communication_via'])));

	//Query for the url link
	$query = _wire_rebuild_get(FALSE, 'url');

	$output .= "<br><table><tr class='wires_messages ".($i%2 == 0 ? 'even' : 'odd')."'>";
	$output .= "<td width='50px' valign='top'>";
	$output .= "<a href=".$dir_link.">".$avatar."</a><br/>";
	$output .= "</td>";
	$output .= "<td valign='top'>";
	$output .= _wire_attachments_theme($m['attachments']);
	$output .= $wire->parse_text($m['text']);
	$output .= "</td>";
	$output .= "</tr><tr>";
	$output .= "<td>" . $rewire . "</td>";
	$output .= "<td valign='top' class='wire_posted_by'>". date('y-m-d h:m:s', $m['timestamp']) ." gmt? by ".$posted_by."<br>via: " . $via . "</td>";
	$output .= "</tr></table>";

	return $output;

}

/**
 * Main function to search messages
 */
function wire_messages_search_page(){

	global $wire, $user;

	module_load_include('inc', 'wire', 'wire.conversation');

	drupal_set_title(t('Search messages')); //Set title

	$output = drupal_get_form('_wire_messages_search_form') . "<br><hr><br>";

	if($_GET['text']){

		$query = array();

		$stuff['ini'] = $_GET['ini'] ? $_GET['ini'] : 0;

		//Internal searches
		if($_GET['search_type'] == 0){

			$results = $wire->messages_search($_GET['text'], $user->uid, FALSE, $stuff);

			if($results){

				$messages = wire_theme_conversation($results, 'w');

				$output .= $messages;

				$query['text']        = $_GET['text'];
				$query['search_type'] = $_GET['search_type'];

			}

		}
		else{ //Remote searches

			$messages = $wire->remote_messages_search($_GET['text'], $stuff);

			//Parse messages if it is not an error
			if(is_array($messages)){

				foreach($messages as $message){
					$results .= _wire_theme_messages_remote($message);
				}

				$output .= $results;

			}

		}

		//If there are results add a pager to find more of them
		if($results){
			//$output .= $results;
			//$output .= _wire_pager('wire/search/messages', ($_GET['ini'] ? $_GET['ini'] : 0), $query);
		}
		else{
			$output .= t("<h1>Sorry, but no results where found. Try something else maybe.</h1>");
		}

	}

	return $output;

}

