<?php

/**
 * @file
 *  Wired conversations
 */

//Regular view, no children
define('VIEW_TYPE_NORMAL',  0);

//Threaded conversations
define('VIEW_TYPE_THREADED',  1);

/**
 * Helper function to add messages from a form
 */
function _wire_add_message(&$form_state, $submit = FALSE){

	global $wire;

	$form['uid']          = $form_state['values']['uid'];
	$form['status']       = $form_state['values']['status'];
	$form['contact_cid']  = $form_state['values']['contact_cid'];
	$form['author']       = $form_state['values']['author'];
	$form['timestamp']    = $form_state['values']['time'];
	$form['text']         = $form_state['values']['message_text'];
	$form['parent']       = $form_state['values']['parent'];
	$form['attachments']  = $form_state['values']['attachments'];
	$form['mkey']         = $form_state['values']['mkey'];
	$form['type']         = $form_state['values']['type'];
	$form['channel']      = $form_state['values']['channel'];
	$form['commkey']      = $_SERVER['SERVER_NAME'];

	$form['from']         = $form_state['values']['from'];
	$form['wire_address'] = $form_state['values']['wire_address'];

	return $wire->add_message($form, $submit);

}

/**
 * Form for new messages
 * @param details an array with the following information
 *  -cid
 *  -wire_address
 *  -from
 *  -type = '1::pm|w'
 *  -author = FALSE
 *  -mkey = FALSE
 *  -parent = 0
 */
function _wire_form_message_new($form_state, $details = array()){

	//Defaults
	if(!isset($details['type'])){
		$details['type'] = '1::pm';
	}

	if(!isset($details['parent'])){
		$details['parent'] = 0;
	}

	$form['#attributes'] = array(
		'enctype' => "multipart/form-data",
	);

	$get = _wire_rebuild_get();

	$form['#redirect'] = array($_GET['q'], $get);

	$form['wire_attachments'] = array(
		'#type'        => 'fieldset',
		'#title'       => t('Attachments'),
		'#collapsible' => TRUE,
		'#collapsed'   => TRUE
	);

	$form['wire_attachments']['attachment'] = array(
		'#type'        => 'file',
		'#size'        => 48,
		'#description' => t('You can attach a file of any type (Except excecutables)'),
	);

	$form['message_text'] = array(
		'#type'        => 'textarea',
		'#cols'        => 60,
		'#rows'        => 1,
		);

		$form['contact_cid'] = array(
			'#type'  => 'hidden',
			'#value' => $details['cid'],
		);

		$form['wire_address'] = array(
			'#type'  => 'hidden',
			'#value' => $details['wire_address'],
		);

		$form['author'] = array(
			'#type'  => 'hidden',
			'#value' => $details['author'],
		);

		$form['parent'] = array(
			'#type'  => 'hidden',
			'#value' => $details['parent'],
		);

		$form['type'] = array(
			'#type'  => 'hidden',
			'#value' => $details['type'],
		);

		$form['from'] = array(
			'#type'  => 'hidden',
			'#value' => $details['from'],
		);

		$form['mkey'] = array(
			'#type'  => 'hidden',
			'#value' => ($details['mkey'] ? $details['mkey'] : _wire_mkey_messages()),
		);

		$form['submit'] = array(
			'#type'  => 'submit',
			'#value' => t('Send')
		);

		return $form;

	}

/**
 * Processes the new message submission form.
 */
function _wire_form_message_new_submit($form, &$form_state){

	global $user, $wire;

	$type = explode('::', $form_state['values']['type']);

	$form_state['values']['uid']     = $user->uid;

	$form_state['values']['type']    = $type[0];
	$form_state['values']['channel'] = $type[1];

	$form_state['values']['time'] = time();

	$submit = FALSE;

	//Tell the other party about the message if it is a wire or a pm to someone else
	if($form_state['values']['wire_name'] != wire_member_name() || $type[1] == 'w'){
		$submit = TRUE;
	}

	$r = _wire_add_message($form_state, $submit);

}

/**
 * Validates the new message form and uploads files if required
 */
function _wire_form_message_new_validate($form, &$form_state){

	global $user;

	//See if the message has content
	if($form_state['values']['message_text'] == '' && $_FILES['files']['name']['attachment'] == ''){

		form_set_error('message_text', t('You need to say something or send something. The message can not be empty.'));

	}

	$upload_dir = wire_attachments('absolute', $user->uid);

	//Confirm that the dir exists
	if(!is_dir($upload_dir)){
		mkdir($upload_dir);
		chmod($upload_dir, 0777);
	}

	if($_FILES['files']['name']['attachment'] != ''){
		$file = file_save_upload('attachment', NULL, $upload_dir);

		if(!$file){
			form_set_error('upload', 'Some error occured while uploading your file');
		}
		else{
			$form_state['values']['attachments'] = array(wire_attachments('mask', $user->uid) . basename($file->filepath));
		}

		$form_state['wire_file_state'] = $file;
	}

}

/**
 * Helper function - Get attachment file types
 * @todo
 *  Select the best file to hold this function
 */
function _wire_attachments_get_type($file){

	$file = basename($file);

	//Only consider jpg jpeg gif png	
	if(strpos($file, '.jpg') !== FALSE || strpos($file, '.gif') !== FALSE || strpos($file, 'jpeg') !== FALSE || strpos($file, 'png') !== FALSE){
		return FILE_TYPE_IMAGE;
	}

	//Only consider ogg
	if(strpos($file, '.ogg')){
		return FILE_TYPE_PLAYABLE;
	}

	//Only consider mp3 mp4
	if(strpos($file, '.mp4') !== FALSE || strpos($file, '.mp3') !== FALSE){
		return FILE_TYPE_AUDIO;
	}

	//Only consider flv mp4 avi
	if(strpos($file, '.avi') !== FALSE || strpos($file, '.mp4') !== FALSE || strpos($file, '.flv') !== FALSE){
		return FILE_TYPE_VIDEO;
	}

	return FILE_TYPE_FILE;

}

/**
 * Themes a conversation
 */
function wire_theme_conversation($messages, $channel = 'pm'){

	global $user, $wire;

	//Users information in order to set the correct avatars
	$this_user = $wire->get_contact_info(wire_member_name());

	if($messages){

		//$output .= "<div id='wire_conversation'><table id='wire_conversation_table' border=1>";

		$i = 0;

		foreach($messages as $message => $m){

			$output .= _wire_theme_conversation($m, $channel, $this_user);

			//This is NOT recursive, it only works with one level.
			if(is_array($m['children'])){
				foreach($m['children'] as $children => $child){
					$output .= _wire_theme_conversation($child, $channel, $this_user, 1);
				}
			}

			$i++;
		}

//		$output .= "</table></div>";
	}

	return $output;

}

/**
 * Helper function to theme messages
 */
function _wire_theme_conversation($m, $channel, $this_user, $level = 0){

	global $wire;

	$rewire = ($channel == 'w' ? l("Rewire", "wire/rewire/message/" . $m['mkey'] . "/" . $m['author_info']['cid']) . " | " : "");

	$reply = '<a href="'. ($channel == 'pm' ? '/wire/talk' : 'wire') . '&parent=' . $m['mkey'] . '&reply=1' . '&wire_address=' . $_GET['wire_address'] . '">Reply</a>' . " | ";

	$response_to = ($m['parent'] == 0
	  					? ''
					  	: sprintf('In response to %s',
							l(substr($m['parent'], 0, 7),
							'wire' . ($i['channel'] == 'pm' ? '/talk' : ''),
							array('query' => array('parent' => $m['parent'],
															'wire_address' => $m['author_info']['wire_address']
															)
									)
							)
								)
						. ' | ');

	$posted_by = l(($m['author_info']['name'] ? $m['author_info']['name'] : "[ " . $m['author_info']['wire_address'] . " ]"), 'wire/directory/view/find/', array('query' => array('who' => $m['author_info']['wire_address'], 'dir' => TRUE)));

	$talk = ($channel == 'w' ? l('Talk', 'wire/talk/', array('query' => array('wire_address' => $m['author_info']['wire_address']))) . ' | ': '');

	//If this user posted this message
	if($this_user['cid'] == $m['author_info']['cid']){

		//I posted this
		$poster = 'me';

		$avatar = "<img src='" . wire_picture($this_user['picture']) . "' class='wire_avatar_messages'>";

		$dir_link = url('wire/directory/view/find', array('query' => array('who'=> $this_user['wire_address'], 'dir' => TRUE)));
	}
	//If not, we'll put it on the name of the contact
	else{

		//I did not post this
		$poster = 'notme';
		$avatar = "<img src='" . wire_picture($m['contact_info']['picture']) . "' class='wire_avatar_messages'
		  			  title='By ".$m['author_info']['name']."'>";

		$dir_link = url('wire/directory/view/find', array('query' => array('who'=> $m['contact_info']['wire_address'], 'dir' => TRUE)));
	}

	$spacer = ($level > 0 ? "<td>&nbsp</td>" : '');

	$via = l($m['via'], 'http://'.$m['url'], array('attributes' => array('target' => '_blank', 'title' => 'Posted via ' . $m['via'])));

	$query = _wire_rebuild_get(FALSE, 'url');

	$delete = l(t('Delete'), 'wire/delete/message', array('query' => array('mid' => $m['mid'], 'return' => ($channel == 'pm' ? 'wire/talk' : 'wire'), 'query' => $query)));

	$output .= "<div id='wire_conversation'><table id='wire_conversation_table'>";

	$output .= "<tr class='wires_messages ".($i%2 == 0 ? 'even' : 'odd')."'>";
	$output .= $spacer;

	//If the poster was this user we'll put his/her picture to the left
	if($poster == 'me'){
	$output .= "<td width='50px' valign='top'>";
	$output .= "<a href=".$dir_link.">".$avatar."</a><br/>";
	$output .= "</td>";
	}

	$output .= "<td valign='top'>";
	$output .= _wire_attachments_theme($m['attachments']);
	$output .= is_array($m['attachments']) ? "<br>" : '';
	$output .= _filter_url($wire->parse_text($m['text']), 1);
	$output .= "</td>";

	//When the poster is the other party, or someone else, we'll put the avatar to the right
	if($poster == 'notme'){
	$output .= "<td width='50px' valign='top'>";
	$output .= "<a href=".$dir_link.">".$avatar."</a><br/>";
	$output .= "</td>";
	}

	$output .= "</tr><tr>";
	$output .= $spacer;
	$output .= "<td colspan=2 align='right' class='wire_post_tools'>";
	$output .= "<div class='wire_posted_by'><span class='wire_posted_date'> " . date('D M h:i:s a', $m['timestamp']) ."</span> <span class='wire_posted_by'> " . $posted_by . " </span> via: <span class='wire_posted_via'>" . $via . "</span></div>";
	$output .= $response_to;
	$output .= $talk . $rewire . $reply . $delete . "</td>";
	$output .= "</tr>";

	$output .= "</table></div>";

	return $output;

}

/**
 * Helper function - Themes files attachments for messages
 */
function _wire_attachments_theme($attachments = array()){

	//not really working
	if(count($attachments) == 0){
		return FALSE;
	}

	$icons = WIRE_SERVER .'/'. drupal_get_path('module', 'wire') . '/imgs';

	$format = FALSE;

	//Popup window link
	$popup = 'onclick="return popitup("index.php")"';

	foreach($attachments as $attachment => $file){

		$media_link = 'wire/mediaplayer';

		$type = _wire_attachments_get_type($file);

		if($type == FILE_TYPE_IMAGE){

		/*	$format = sprintf('<a href="#" onClick="return popitup(\'%s\', \'\', \'width=400,height=600\')"><img src="%s" class="wire_imgs""></a>',
			url($media_link, array('query' => array('media' => $file, 'type' => 'img'))), $file);*/
			$format = sprintf('<div class="wire_attached_img" style="float: left;"><a href="%s" target="_blank"><img src="%s" class="wire_imgs""></a><br><span class="wire_img_clicktoview">%s</span></div>', $file, $file, t('Click the image for full view <br /> (Opens in new window)'));
		}
/*
		elseif($type == FILE_TYPE_AUDIO){
			$format = sprintf('<a href="#"><img src="%s/audio.png" title="%s" alt="%s"></a>', $file, $icons, $file, $file);
		}
 */
		//elseif($type == FILE_TYPE_FILE){
		////I'll format any other attachments as generic files, I'll include flowplayer in feature versions
		else{
			$information = t(sprintf('<hr>Careful when opening file: <br> %s', $file));
			//$format = sprintf('<div style="float: right;"><a href="%s" target="_blank"><img src="%s/general.png" title="%s" alt="%s" style="float: right;"></a>%s</div>', $file, $icons, $file, $file, $information);
				$format = sprintf('<div style="float: left; width:200px;"><a href="%s" target="_blank"><img src="%s/general.png" title="%s" alt="%s""></a>%s</div>', $file, $icons, $file, $file, $information);
		}

/*		elseif($type == FILE_TYPE_PLAYABLE){
			$format = sprintf("<video src='%s' controls='controls'>", $file);
			//$format = sprintf('<a href="#" onClick="return popitup(\'%s\', \'\', \'width=400,height=600\')"><img src="%s/general.png" class="wire_imgs""></a>',
			  //						url($media_link, array('query' => array('media' => $file, 'type' => 'img'))), $file);
}*/

	}

	return $format;

}

/**
 * Deletes messages
 */
function wire_messages_delete(){

//	module_load_include('inc', 'wire', 'wire.contacts');

	global $user, $wire;

	//Just for security
	if(!$_GET['mid']){
		drupal_goto('<front>');
	}

	//Confirm that the message belongs to the user or that the user has priviledges
	$message = $wire->get_messages(FALSE, FALSE, array('mid' => $_GET['mid']));
	
	//Since this will return an array with arrays of messages
	$message = $message[0];

	//Confirm that the message was posted by this user or that it is an admin
	if($user->uid == $message['uid'] || user_access('admin wire')){

		$notify = FALSE;
		 //This can also be verified by cid
		if($message['channel'] == 'pm' && $message['author_info']['wire_address'] == wire_member_name()){
			$notify = TRUE;
		}

		//delete message and notify if required
		$r = $wire->messages_delete($message);

		if($r && is_array($message['attachments'])){
			//Delete attachment
			foreach($message['attachments'] as $attachment => $file){
				//Get the correct path for the file
				$file_name = wire_attachments('absolute') . basename($file);
				wire_delete_stuff($file_name);
			}
			drupal_set_message(t('Message succesfully deleted'));
		}

	}


	drupal_goto($_GET['return'], $_GET['query']);

}

/**
 * Rewires messages
 */
function wire_messages_rewire(){

	global $user, $wire;

	//Information about this user
	$who = wire_member_name();

	//Contact information
	$contact = $wire->get_contact_info($who);

	//Get details about the message
	$message = $wire->get_messages(FALSE, arg(3), array('mkey' => arg(2)));

	if($message){
		$message = $message[0];

		$form['uid']         = $user->uid;
		$form['contact_cid'] = $contact['cid'];
		$form['author']      = $message['author'];
		$form['timestamp']   = time();
		$form['text']        = $message['text'];

		$form['attachments'] = $message['attachments'];
		$form['mkey']        = $message['mkey'];
		$form['status']      = 0;
		$form['type']        = $message['type'];
		$form['channel'] 	 = 'w';
		$form['commkey']     = variable_get('wire_comm_key', '');

		$form['from'] 	 	 = $who;

		$r = $wire->add_message($form, TRUE);

		drupal_set_message(t('Message re-wired'));
	}

	drupal_goto('wire');

}

function _wire_view_public(){

	$query['user_name'] = arg(1);

	//The messages retrieving path in drupal
	$messages_path = url("wire/view/public", array('query' => $query));

	$refresh_messages = "
		 $(document).ready(function() {
			 $('#wire_messages_container').load('".$messages_path."');
			var refreshId = setInterval(function() {
			  $('#wire_messages_container').load('".$messages_path."');
			}, ".variable_get('wire_refresh_rate_contacts', 5000).");
			$.ajaxSetup({ cache: false });
		});";

	drupal_add_js($refresh_messages, 'inline');

	$output .= "<div style='border: #e5e7e7 1px solid;' id='wire_messages_container'></div>";

	$output .= _wire_pager(($channel == 'w' ? 'wire' : 'wire/talk'), ($_GET['ini'] ? $_GET['ini'] : 0), $query);

	return $output;

}

function _wire_view(){

	global $wire, $user;

	$form_details = FALSE;

	//Lets set some basic stuff

	//Get the wire address, it can be another contact or it can be him self 
	$wire_address = ($_GET['wire_address'] ? $_GET['wire_address'] : wire_member_name());

	//Is this a wire or a pm
	$channel = (arg(1) == 'talk' ? 'pm' : 'w');

	//Is this a logged in user?
	$uid = $user->uid;

	//Mode
	/// @todo Improve this
	$mode = isset($_GET['mode']) ? $_GET['mode'] : 0;

	//Get contact information
	$contact = $wire->get_contact_info($wire_address);

	//If the contact is this user and there is no contact info yet, we'll create it
	if(!$contact && $wire_address == wire_member_name()){

		$info['uid']          = $user->uid;
		$info['wire_address'] = $wire_address;
		$info['name']         = '';
		$info['unique_id']    = '';
		$info['about']        = '';
		$info['token']        = $user->name;
		$info['picture']      = '';
		$info['status']       = 0;

		$wire->create_contact($info);

		$contact = $wire->get_contact_info($wire_address);

	}

	//Go away if the contact does not exist and it is an external contact
	if(!$contact){
		return MENU_NOT_FOUND;
	}

	//Query for the other page
	$query  = _wire_rebuild_get(FALSE, 'url'); 

	//Update contact information if required
	if($contact['status'] == 1){
		$wire->profiles_update_pending(array($contact));
	}

	$this_user = $wire->get_contact_info(wire_member_name());

	//Private messages
	if($channel == 'pm'){

		//Go away if you are not logged in
		if($uid == 0){
			drupal_goto('<front>');
		}

		//You must be a contact if you want to speak with this person
		if(!$wire->confirm_rel($contact['cid'], $user->uid, 3)){
			drupal_goto('<front>');
		}

		//Information required for the message form
		$form_details = array(
			'cid'          => $contact['cid'],
			'wire_address' => $contact['wire_address'],
			'type'         => '1::' . $channel,
			'from'         => wire_member_name(),
			'author'       => $this_user['cid'],
			'mkey'         => $mkey,
			'parent'       => $_GET['parent']
		);

		//Always talking to a contact	
		$query['cid']     = $contact['cid'];

		drupal_set_title("Conversation");

	}
	else{ //Wire Channels

		//You want to see someones wire
		if($uid != $contact['uid']){

			//See if the contact is an account here
			$to = explode('::', $wire_address);
			$account = wire_confirm_account($to[0], 1);
		 	$query['cid'] = $contact['cid'];

			//Anonymous users can only see wires from users of this client who's wire is public (not really implemented at this point)
			if(!$account){
				return MENU_ACCESS_DENIED;
			}

		}

		//Must be logged in, be able to send wires and be a contact with the proper permissions
		if($uid > 0 && user_access('send wires')){
			//Information required for the message form
			$form_details = array(
				'cid'          => $contact['cid'],
				'wire_address' => $contact['wire_address'],
				'type'         => '1::' . $channel,
				'from'         => wire_member_name(),
				'author'       => $this_user['cid'],
				'mkey'         => $mkey,
				'parent'       => $_GET['parent']
			);
		}

	}

	//Possition the cursor in the new message box by default
	$focus_pos = "window.onload = function() {
	  document.getElementById('edit-message-text').focus();
	};
	";

	//Query for the messages retrieving page
	$query['channel'] = $channel;
	$query['ini']     = $_GET['ini'] ? $_GET['ini'] : 0; 
	$query['contact'] = $wire_address;
	$query['user']    = $this_user['wire_address'];
	$query['mode']		= $mode;
	$query['uid']     = $this_user['uid'];

	//The messages retrieving path in drupal
	$messages_path = url("wire/talk/conversation", array('query' => $query));

	$refresh_messages = "
		 $(document).ready(function() {
			 $('#wire_messages_container').load('".$messages_path."');
			var refreshId = setInterval(function() {
			  $('#wire_messages_container').load('".$messages_path."');
			}, ".variable_get('wire_refresh_rate_contacts', 5000).");
			$.ajaxSetup({ cache: false });
		});";

	//Actually add the scripts to the page
	drupal_add_js($focus_pos,        'inline');
	drupal_add_js($refresh_messages, 'inline');

	//Set the new message form when it applies
	if(user_access('send wires') && $form_details){
		$output = drupal_get_form('_wire_form_message_new', $form_details) . "<br />";
	}

	$output .= "<div style='border: #e5e7e7 1px solid;' id='wire_messages_container'></div>";

	$output .= _wire_pager(($channel == 'w' ? 'wire' : 'wire/talk'), ($_GET['ini'] ? $_GET['ini'] : 0), $query);

	return $output;

}

/**
 * @brief Main Wirez and conversations function
 * @return A themed page with the messages and a form to send new messages when it applies
 */
function wire_wire(){

	global $wire, $user;

	//If no wire address was indicated will assume you just want to see someones wire
	if(!$_GET['wire_address'] && arg(1) != ''){
		return _wire_view_public();
	}
	else{
		return _wire_view();
	}

}


